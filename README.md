# This has been superseded by the README on the new AGS repository

# Assets for the the AGS Data format web site.

## The web site is in progress. The todo list is shown below:

## List updated 27/03/18 JAJB

- [ ] Config live system
- [x] Transfer pilot config to main site
- [ ] Transfer forum posts
- [ ] Sort out content
- [x] Sort out menu structure
- [ ] Testing
- [ ] Create new website subgroup email for use with gitlab
- [ ] Set up gitlab for AGS (dfwg-web@ags.org.uk)
- [ ] Set up admin aprovals
- [x] Provide previous versions (4)
- [ ] Provide diffs for versions
- [ ] create markdown content
- [ ] Create P code form and database table
- [x] Get full version of datatables!!!!!
- [x] Put Guidance docs on gitlab
- [x] Get content from Jackie - ERES & ABBR codes done to 26/03/18
- [ ] Sort backup for gitlab
- [ ] Redirect agsdataformat.com to ags.org.uk/dataformat
- [x] Check with AGS they are happy to authorise DF users permission on website
- [ ] Add page to request access to dataformat section of ags.org.uk and send email to ags.org.uk for them to action for us


## Pages:

- [ ] Data Format - Contains introduction/press releases
    -    [ ] News
    -    [ ] AGS 4
        -    [ ] Download Document
        -    [ ] Groups & Headings
        -    [ ] Rules
        -    [ ] Guidance
        -    [ ] Examples
        -    [ ] Abbreviations/Heading (ABBR) Code List
        -    [ ] Request a new ABBR Code
        -    [ ] Geoenvironmental (ERES) Code List
        -    [ ] Request a new ERES Code
        -    [ ] Request a new Heading (DICT group)
    -    [ ] Discussion Forum
    -    [ ] Registered Organisations
    -    [ ] Become a Registered Organisation
    -    [ ] Legacy Formats
        -    [ ] AGS 3
        -    [ ] Introduction
        -    [ ] Examples
        -    [ ] Guidelines
        -    [ ] Groups and Headings
        -    [ ] Abbreviations
        -    [ ] Contamination Codes
        -    [ ] Download
        -    [ ] AGS 2
        -    [ ] AGS 1
    -    [ ] Software (list)

## Common

- [ ] Q&A - import old
- [ ] Conversion Tools

